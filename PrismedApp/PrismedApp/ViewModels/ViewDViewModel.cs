﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PrismedApp.ViewModels
{
    public class ViewDViewModel : ViewModelBase
    {
        public ViewDViewModel(INavigationService navigationService)
            : base(navigationService)
        {

        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine($"OnNavigatedFrom() was called on {nameof(ViewDViewModel)}; parameters {parameters}");
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Debug.WriteLine($"OnNavigatedTo() was called on {nameof(ViewDViewModel)}; parameters {parameters}");
        }
    }
}
