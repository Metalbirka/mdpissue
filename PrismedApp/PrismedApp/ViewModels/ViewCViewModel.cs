﻿using Prism.Navigation;
using System.Diagnostics;

namespace PrismedApp.ViewModels
{
    public class ViewCViewModel : ViewModelBase
    {
        public ViewCViewModel(INavigationService navigationService)
            : base(navigationService)
        {

        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine($"OnNavigatedFrom() was called on {nameof(ViewCViewModel)}; parameters {parameters}");
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Debug.WriteLine($"OnNavigatedTo() was called on {nameof(ViewCViewModel)}; parameters {parameters}");
        }
    }
}
