﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;

namespace PrismedApp.ViewModels
{
    public class MainPageViewModel : BindableBase
    {
        public MainPageViewModel(INavigationService navigationService)
        {
            NavigateCommand = new DelegateCommand<string>(s =>
                  navigationService.NavigateAsync($"{nameof(NavigationPage)}/{s}"));
        }

        public DelegateCommand<string> NavigateCommand { get; set; }
    }
}